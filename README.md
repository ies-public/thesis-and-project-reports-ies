# Thesis and Project Reports IES

To download this repository use the command

```
git clone git@git.rwth-aachen.de:ies-public/thesis-and-project-reports-ies.git

```

## Folder Hirachy in this Repository

```
thesis-and-project-reports-ies
.
|-- Creating Schematics and Images
|   |-- Inkscape
|   |   `-- Inkscape_template.svg
|   |-- Tikz
|   |   `-- empty
|   `-- Visio
|       |-- CMOSEdu.vss
|       |-- EE - RAZAVI.vss
|       `-- SL_Thesis.vssx
|-- Implementation
    |-- PEMSkript_markiert.pdf
|   `-- IES-Project-Guide.pdf 
|-- README.md
`-- Writing your Thesis
    |-- Groessen_Einheiten_Gleichungen.pdf
    |-- How_To_IES_Thesis.pdf
    `-- Logos
        `-- ies_logo.eps

```

## Phases of your Thesis / Seminar
1. Clarification of your Topic
    - E.g. Write a Task List
    - Which specs must be satisfied in the end?
    - Which resources do you have?
    - ... 
2. Concept Phase
    - Do research reading papers.
    - Creation of top-level schematics
    - Testing out how the technology behaves
    - Go back to Clarification Phase?!
    - ...
3. Design Phase
    - Step by step developing your sub-blocks
    - Testing!!!
    - Go Back to Concept Phase?!
    - ...
4. Testing and Verification
    - Verify every relevant characteristic of your Design
    - Test your device
    - Think of all possible edgecases your design should be running in.
    - Go back to Design Phase ?!
    - ...
5. Writing your Report
    - Create beautiful schemetics using Inkscape
    - Create beautiful plots (e.g. using Veusz)
    - Write and review your own text.
    - Let many people review your Text.
    - ...



## PCB Design
You can find everything relevant for PCB Design in the following Gitlab:
https://git.rwth-aachen.de/ies-students/platinen_design
If you have issues accessing it please contact your supervisor!

## Reports
- Look into the IES-Project_Guide in the folder "Implementation" to find information about a good writing style and structure of your thesis.
- Look at the File How_To_IES_Thesis.pdf in the folder "Writing your Thesis".
- You can find more information about the Latex templates on the TU Website: https://www.intern.tu-darmstadt.de/arbeitsmittel/dokumente_formulare/sortiert_von_a_bis_z/details_147072.de.jsp
## Presentations
- Duration of Presentations:
    - Proseminar: 10 min
    - Projektseminar, Seminar A & B: 15 min
    - PEM II bis IV: 15 min
    - Bachelor- & Masterthesis: 20 min
- Template: https://www.intern.tu-darmstadt.de/arbeitsmittel/dokumente_formulare/sortiert_von_a_bis_z/details_106368.de.jsp

